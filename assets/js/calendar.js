



$(document).ready(function() {

    var custom_event;

	
    var dragging = false;
    $("body").on("touchstart", function(){
        dragging = false;
    });
    $("body").bind('touchmove', function(event) {
        dragging = true;
    });

    //if(Modernizr.touch){ custom_event = "tap" }else{ custom_event = "click" }
    if(Modernizr.touch){
        if (dragging)
        {
            custom_event = "click"
        }
        else
        {
            custom_event = "touchend"
        }
    }
    else
    {
        custom_event = "click"
		

    }



    /*
    Takvim
    *******/
    function changeMonth(direction,today,newMonth,newYear)
    {
        var prevMonthText = $(".calendarMonths .changeMonth.prev");
        var currentText = $(".calendarMonths .currentMonth");
        var nextMonthText = $(".calendarMonths .changeMonth.next");
        var diff;
        var currentDate;
        var prevDate;
        var nextDate;

        currentDate = moment(newYear+"-"+newMonth+"-01");
        prevDate = moment(currentDate).subtract(1, 'month');
        nextDate = moment(currentDate).add(1, 'month');

        diff = currentDate.diff(today, 'month');
        if(diff > -1)
        {
            newMonth = moment(currentDate).format("MMMM");
            newYear = moment(currentDate).format("YYYY");
            currentText.text(newMonth+" "+newYear);

            currentText.attr("data-month",moment(currentDate).format("MM"));
            currentText.attr("data-year",moment(currentDate).format("YYYY"));
            prevMonthText.attr("data-month",moment(prevDate).format("MM"));
            prevMonthText.attr("data-year",moment(prevDate).format("YYYY"));
            nextMonthText.attr("data-month",moment(nextDate).format("MM"));
            nextMonthText.attr("data-year",moment(nextDate).format("YYYY"));

            var last = moment(currentDate).add(1, 'month').date(0).format("DD");
            var first;
            if(diff == "-0" && moment(today).format("MM") == moment(currentDate).format("MM"))
            {
                first = moment(today).format("D");
            }
            else
            {
                first = 1;
            }
            $(document).find(".daysInMonths ul").empty();
            for(i = first; i <= last; i++)
            {
                var date = moment(currentDate).date(i).format("YYYY-MM-DD");
                listDays(date);
                if(i == last)
                {
                    listEventsDots();
                }
            }
        }

    }
	
    function listEventsDots(){
        var list = $(document).find(".daysInMonths ul");
        list.find("li").eq(0).addClass("active");
        var events = [
			
			/* Nisan */
			
            {"id":1,"date":"2015-04-04","title":"Deneme 1"},
            {"id":1,"date":"2015-04-04","title":"Deneme 1"},
            {"id":1,"date":"2015-04-06","title":"Deneme 1"},
            {"id":1,"date":"2015-04-09","title":"Deneme 1"},
            {"id":1,"date":"2015-04-11","title":"Deneme 1"},
            {"id":1,"date":"2015-04-11","title":"Deneme 1"},
            {"id":1,"date":"2015-04-13","title":"Deneme 1"},
            {"id":1,"date":"2015-04-15","title":"Deneme 1"},
            {"id":1,"date":"2015-04-16","title":"Deneme 1"},
            {"id":1,"date":"2015-04-18","title":"Deneme 1"},
            {"id":1,"date":"2015-04-18","title":"Deneme 1"},
            {"id":1,"date":"2015-04-20","title":"Deneme 1"},
            {"id":1,"date":"2015-04-22","title":"Deneme 1"},
            {"id":1,"date":"2015-04-25","title":"Deneme 1"},
            {"id":1,"date":"2015-04-25","title":"Deneme 1"},
            {"id":1,"date":"2015-04-27","title":"Deneme 1"},
            {"id":1,"date":"2015-04-29","title":"Deneme 1"},
			
        ];

        $.each(events,function(index,event){
            var eventDate = event.date;
            var eventTitle = event.title;

            var point = '<a class="event-dot" data-title="'+eventTitle+'" data-date="'+eventDate+'"></a>';
            list.find("li[data-date="+eventDate+"] .eventsDots").append(point);
        });
    }
    function listDays(date){
        var dayName = moment(date).format("ddd");
        var dayNumber = moment(date).format("D");

        var dd = moment(date).format("DD");
        var mm = moment(date).format("MM");
        var yyyy = moment(date).format("YYYY");

        var list = $(document).find(".daysInMonths ul");

        var item = '<li id="tarih_'+yyyy+mm+dd+'" data-date="'+date+'" data-year="'+yyyy+'" data-month="'+mm+'" data-day="'+dd+'" class="dateitem"><div class="dayName">'+dayName+'</div><div id="eventsDots" class="eventsDots"></div><div class="dayNumber">'+dayNumber+'</div></li>';
        list.append(item);

        var width = 0;
        $(".daysInMonths li").each(function() {
            width += $(this).outerWidth();
        });
        $(".daysInMonths ul").css({
            width: width + "px"
        });
        var breadcrumps = new IScroll(".daysInMonths", {
            mouseWheel: true,
            scrollX: true,
            scrollY: false
        });
    }
    if($(document).find(".calendarMonths").length > 0)
    {
        $(document).find(".calendarMonths").ready(function(){
            var today = moment($(".calendarMonths").attr("data-today"));
            var first = moment(today).format("D");
            var last = moment(today).add(1, 'month').date(0).format("DD");
            $(document).find(".daysInMonths ul").empty();
            for(i = first; i <= last; i++)
            {
                var date = moment(today).date(i).format("YYYY-MM-DD");
                listDays(date);
                if(i == last)
                {
                    listEventsDots();
                }
            }
        });
    }
    $(document).on(custom_event,".calendarMonths .changeMonth",function(event){
        event.preventDefault();
        var today = moment($(".calendarMonths").attr("data-today"));
        var newMonth = $(this).attr("data-month");
        var newYear = $(this).attr("data-year");

        if($(this).hasClass("prev"))
        {
            changeMonth("prev",today,newMonth,newYear);
        }
        else
        {
            changeMonth("next",today,newMonth,newYear);
        }
    });
    $(document).find(".calendarMonths").swipe({
        swipeLeft:function(){
            var nextItem = $(document).find(".calendarMonths .changeMonth.next");
            var item = $(document).find(".calendarMonths");
            var today = moment(item.attr("data-today"));
            var newMonth = nextItem.attr("data-month");
            var newYear = nextItem.attr("data-year");
            changeMonth("next",today,newMonth,newYear);
        },
        swipeRight:function(){
            var prevItem = $(document).find(".calendarMonths .changeMonth.next");
            var item = $(document).find(".calendarMonths");
            var today = moment(item.attr("data-today"));
            var newMonth = prevItem.attr("data-month");
            var newYear = prevItem.attr("data-year");
            changeMonth("prev",today,newMonth,newYear);
        },
        threshold:25
    });
    $(document).on(custom_event,".daysInMonths ul li",function(event){
        event.preventDefault();
        $(this).parents("ul").find("li").removeClass("active");
        $(this).addClass("active");
    });
	
	
        $(document).on(custom_event, ".daysInMonths ul li:has(a)", function (event) {
          //window.location.href = "?="+$(this).attr("data-date");	
        });
		
	

		
	$(document).on(custom_event,"#tarih_20150404",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150404").fadeIn();
    });
	
	$(document).on(custom_event,"#tarih_20150406",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150406").fadeIn();
    });
	
	$(document).on(custom_event,"#tarih_20150409",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150409").fadeIn();
    });
	
	$(document).on(custom_event,"#tarih_20150411",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150411").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150413",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150413").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150415",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150415").fadeIn();
    });	

	
	$(document).on(custom_event,"#tarih_20150416",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150416").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150418",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150418").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150420",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150420").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150422",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150422").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150425",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150425").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150427",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150427").fadeIn();
    });	
	
	$(document).on(custom_event,"#tarih_20150429",function(event){
        event.preventDefault();
        $(".workshop-detail").hide();
		$(".tarih_20150429").fadeIn();
    });
	
	$(document).on(custom_event,function(event){
		
		/* ��erik Olmayan G�nleri T�klanmaz Yapar */
		
		$("#tarih_20150401").click(false);
		$("#tarih_20150402").click(false);
		$("#tarih_20150403").click(false);
		$("#tarih_20150405").click(false);
		$("#tarih_20150407").click(false);
		$("#tarih_20150408").click(false);
		$("#tarih_20150410").click(false);
		$("#tarih_20150412").click(false);
		$("#tarih_20150414").click(false);
		$("#tarih_20150417").click(false);
		$("#tarih_20150419").click(false);
		$("#tarih_20150421").click(false);
		$("#tarih_20150423").click(false);
		$("#tarih_20150424").click(false);
		$("#tarih_20150426").click(false);
		$("#tarih_20150428").click(false);
		$("#tarih_20150430").click(false);
	
		
	
	});
	
	
});//ready