$(document).ready(function () {

    var custom_event;
    var dragging = false;
    $("body").on("touchstart", function () {
        dragging = false;
    });
    $("body").bind('touchmove', function (event) {
        dragging = true;
    });
    $(window).scroll(function (event) {

        if ($("body").attr("id") == "homepage") {
            var height = $(window).height();
            var scrollY = $(window).scrollTop();
            var header = $("header");
            var wrapper = $("#wrapper");
            if (scrollY > height) {
                header.addClass("fixed");
                wrapper.addClass("fixed");
                event.preventDefault();
            }
            else {
                header.removeClass("fixed");
                wrapper.removeClass("fixed");
                event.preventDefault();
            }
        }
    });

    if (Modernizr.touch) {
        if (dragging) {
            custom_event = "click"
        }
        else {
            custom_event = "touchend"
        }
    }
    else {
        custom_event = "click"
    }

    $(window).load(function () {
    });

    $.fn.select = function () {
        var $this = $(this);
        var classes = $(this).attr("class");
        if (classes == undefined || classes == "undefined") {
            classes = "";
        }
        var val = $("option:selected", this).text();
        var parent = $(this).parent();

        var $content = '<div class="masked-select placeholder ' + classes + '"><span class="masked-select-text" href="javascript:void(0);">' + val + '</span></div>';
        $this.after($content);

        $this.clone().prependTo($this.next(".masked-select")).fadeTo(0, 0.0).removeAttr("class");
        $this.remove();
    };
    $("select").each(function () {
        $(this).select();
    });
    $("select").change(function () {
        var new_val = $("option:selected", this).text();
        $(this).parent().removeClass("changed");
        if ($("option:selected", this).index() != 0) {
            $(this).parent().addClass("changed");
        }
        $(this).parent().find(".masked-select-text").text(new_val);
    });
    $("input[type=radio]").each(function () {

        var $this = $(this);
        var name = $(this).attr("name");
        var parent = $(this).parent();

        $content = '<div class="masked-radio" data-name="' + name + '"><div class="hidden-radio"></div></div>';
        $this.after($content);

        $this.clone().appendTo($this.next(".masked-radio").find(".hidden-radio")).fadeTo(0, 0.0).removeAttr("class");
        $this.remove();
    });

    $(".hidden-radio input").change(function () {

        var name = $(this).parent().parent().attr("data-name");
        var $this = $(this).parent().parent();
        var other_radios = $(document).find(".masked-radio[data-name='" + name + "']");

        if ($(this).is(':checked')) {

            other_radios.find('.masked-radio-checked').remove();
            $this.append('<div class="masked-radio-checked"></div>');
        }
        else {

            $this.find('.masked-radio-checked').remove();
        }

    });
    $("input[type=checkbox]").each(function () {

        var $this = $(this);
        var id = $(this).attr("id");
        var parent = $(this).parent();

        var $content = '<div class="masked-checkbox" data-id="' + id + '"><div class="hidden-checkbox"></div></div>';
        $this.after($content);

        $this.clone().appendTo($this.next(".masked-checkbox").find(".hidden-checkbox")).fadeTo(0, 0.0).removeAttr("class");
        $this.remove();
    });
    $(".hidden-checkbox input").change(function () {

        var id = $(this).parent().parent().attr("data-id");
        var $this = $(this).parent().parent();
        var other_radios = $(document).find(".masked-checkbox[data-id=" + id + "]");

        if (!$(this).is(':checked')) {

            $this.find('.masked-checkbox-checked').remove();
        }
        else {
            other_radios.find('.masked-checkbox-checked').remove();
            $this.append('<div class="masked-checkbox-checked"></div>');
        }

    });

    $(".accordion-module").accordion({
        heightStyle: "content",
        collapsible: true,
        animate: false,
        activate: function (event, ui) {
            if (ui.newHeader.position() && !$("html").hasClass("nav-active")) {
                var top = ui.newHeader.position().top;
                $("html, body").animate({ scrollTop: top });
            }
        }
    });
    $(".accordion-module.collapse-all").accordion({
        collapsible: true,
        active: false,
        animate: false,
        activate: function (event, ui) {
            if (ui.newHeader.position() && !$("html").hasClass("nav-active")) {
                var top = ui.newHeader.position().top;
                $("html, body").animate({ scrollTop: top });
            }
        }
    });

    $(document).on(custom_event, "#file-trigger", function (event) {
        var file = $(this).parent().find("input.file");
        file.trigger("click");

        event.preventDefault();
    });
    //easeInOutQuart

    $(document).on("focus", ".input-text,select,textarea", function (event) {
        var fieldset = $(this).parents("fieldset")
        fieldset.addClass("active");
    });
    $(document).on("blur", ".input-text,select,textarea", function (event) {
        var fieldset = $(this).parents("fieldset")
        fieldset.removeClass("active");
    });
    $(document).find(".textarea.limited").each(function () {
        var limit = $(this).attr("data-limit");
        var len = $(this).val().length;
        var rem = parseInt(limit) - parseInt(len);
        if (rem < 0) { rem = 0; }
        $(this).parent().find(".limit").text(rem);
    });
    $(document).on("change input keypress keyup paste", ".textarea.limited", function (event) {
        var limit = $(this).attr("data-limit");
        var len = $(this).val().length;
        var rem = 0;
        if (len >= limit) {
            $(this).parent().find(".limit").text(rem);
            event.stopPropagation();
            return false;
        }
        rem = parseInt(limit) - parseInt(len);
        $(this).parent().find(".limit").text(rem);
    });

    $(document).on(custom_event, ".nav-trigger", function (event) {

        event.preventDefault();
        var wrapper = $("html");
        var nav = $("nav");
        var header = $("header");

        wrapper.toggleClass("nav-active");
        header.toggleClass("nav-active");
        nav.toggleClass("open");
        event.stopPropagation();

    });


    $(document).on(custom_event, function (event) {
        if (dragging) return;
        var wrapper = $("html");
        var nav = $("nav");
        var header = $("header");
        if (wrapper.hasClass("nav-active") && $(event.target).parents("nav").length == 0) {
            if (event.target.nodeName != "NAV") {
                wrapper.removeClass("nav-active");
                header.toggleClass("nav-active");
                nav.removeClass("open");
                event.stopPropagation();
                return false;
            }
        }
    });

    if ($(".homepage-slider ul li").length > 1) {
        var homeSlider = $(".homepage-slider ul").bxSlider({
            mode: "horizontal",
            pager: true,
            controls: false,
            touchEnabled: false
        });
    }



    if ($(".form-wrapper .text-module").length > 1) {
        var FormSlider = $(".form-wrapper").bxSlider({
            mode: "horizontal",
            pager: false,
            controls: false,
            touchEnabled: false,
            adaptiveHeight: true,
            responsive: true,
            onSlideAfter: function () {
                FormSlider.redrawSlider();
            }
        });

        $(function () {
            FormSlider.goToSlide(2);
            GetCourses(20);
        });

        /* APPLICATION FORM */
        var Birthdate = new Date();
        var Courses = $('select[name=courses]');
        var ChefCourses = $('select[name=chefcourses]');
        var Terms = $('select[name=terms]');
        var Hour = $('input[name=hour]:radio');
        var ChefHour = $('input[name=chefhour]:radio');
        var Hours = $('#hours');
        var ChefTerms = $('select[name=chefterms]');
        var ChefHours = $('#chefhours');
        var Age = 0;
        var Radio = '<label class="radio"><div class="masked-radio" data-name="hour"><div class="hidden-radio"><input type="radio" name="hour" style="opacity: 0;"></div></div>{0}</label>';
        var RadioChef = '<label class="radio"><div class="masked-radio" data-name="chefhour"><div class="hidden-radio"><input type="radio" name="chefhour" style="opacity: 0;"></div></div>{0}</label>';
        var RadioDisabled = '<label class="radio"><div class="masked-radio" data-name="hour"><div class="hidden-radio"><input type="radio" disabled="disabled" name="hour" style="opacity: 0;"></div></div>{0} <i style="color:#bf2604">{1}</i></label>';
        var RadioDisabledChef = '<label class="radio"><div class="masked-radio" data-name="chefhour"><div class="hidden-radio"><input type="radio" disabled="disabled" name="chefhour" style="opacity: 0;"></div></div>{0} <i style="color:#bf2604">{1}</i></label>';

        $(".breadcrumps-number ul li").click(function () {
            if (FormSlider.getCurrentSlide() > $(this).index()) {
                FormSlider.goToSlide($(this).index() + 1);
            }
        });

        $("select[name=Uyruk]").change(function () {
            if ($(this).val() == "T.C.") {
                $(".tc-kimlik-no").val("");
                $(".tc-kimlik").show();
            } else {
                $(".tc-kimlik-no").val("-");
                $(".tc-kimlik").hide();
            }
        });

        $("form select,input,textarea").change(function () {
            if ($(this).val() == "") {
                $(this).parents("fieldset").addClass("red");
            } else {
                $(this).parents("fieldset").removeClass("red");
            }
        });

        $("form .button").click(function (e) {
            var Part = $(this).parents("div.form-step");
            var Fieldset = Part.find("fieldset.required:visible");
            var Error = false;

            Fieldset.each(function (i, e) {
                $(e).removeClass("red");
                $("select,input,textarea", e).each(function (x, s) {
                    if ($(s).val() == "") {
                        $(e).addClass("red");
                        Error = true;
                    }
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if ($(s).hasClass("email") && !regex.test($(s).val())) {
                        $(e).addClass("red");
                        Error = true;
                    }
                });
            });

            if (Error) {
                $('html, body').animate({
                    scrollTop: $(".red").offset().top
                }, 500);
                return;
            }
            if (FormSlider.getCurrentSlide() == 1) {
                Birthdate = new Date($('select[name=birthday-year]').val(), $('select[name=birthday-month]').val(), $('select[name=birthday-day]').val());
                Age = GetAge(Birthdate);
                GetCourses(Age);
            }

            if (FormSlider.getCurrentSlide() == 4) {
                $.ajax({
                    type: "POST",
                    url: "/basvuru",
                    data: $("form").serialize(),
                    success: FormSlider.goToNextSlide()
                });
            } else {
                FormSlider.goToNextSlide();
            }
        });

        $("select[name^=birthday-]").change(function () {
            $("#birthdate-hidden").val($("select[name=birthday-day]").val() + "." + $("select[name=birthday-month]").val() + "." + $("select[name=birthday-year]").val())
        });

        ChefCourses.change(function () {
            var ChefTerm = 0;
            ChefHours.parent().show();
            $("#chef-courses-hidden").val($(this).parent().find("span").text());
            $.ajax({
                url: "/term/" + ChefCourses.val() + "/0",
                dataType: 'json',
                async: false,
                success: function (data) {
                    //ChefTerms.find('option').remove().end().append(new Option("Seçiniz", ""));
                    //ChefTerms.next('.masked-select-text').text("Seçiniz");
                    //ChefHours.find('label').remove(); 
                    jQuery.each(data, function (index, itemData) {
                        if (itemData.Title == $("option:selected", Terms).text()) {
                            ChefTerm = itemData.BlockID;
                        }

                        ChefTerms.append(new Option(itemData.Title, itemData.BlockID));
                    });

                    FormSlider.redrawSlider();
                }
            });

            $.ajax({
                url: "/term/" + ChefCourses.val() + "/" + ChefTerm,
                dataType: 'json',
                async: false,
                success: function (data) {
                    ChefHours.find('label').remove();
                    jQuery.each(data.Block, function (index, itemData) {
                        if (GetObject(itemData.Element, "Name", "term-status").Value == "false") {
                            ChefHours.append(RadioDisabledChef.replace("{0}", GetObject(itemData.Element, "Name", "term-title").Value + " - " + GetObject(itemData.Element, "Name", "term-hours").Value).replace("{1}", GetObject(itemData.Element, "Name", "term-status-message").Value));
                        } else {
                            ChefHours.append(RadioChef.replace("{0}", GetObject(itemData.Element, "Name", "term-title").Value + " - " + GetObject(itemData.Element, "Name", "term-hours").Value));
                        }
                    });

                    FormSlider.redrawSlider();
                }
            });




            ChefHour = $('input[name=chefhour]:radio');
            ChefHour.change(function () {
                $("#chef-date-hidden").val($(this).parents("label").text());
                $("input[data-name=chefhour]").val($(this).parents("label").text());
            });


            $(".hidden-radio input").change(function () {
                var name = $(this).parent().parent().attr("data-name");
                var $this = $(this).parent().parent();
                var other_radios = $(document).find(".masked-radio[data-name='" + name + "']");

                if ($(this).is(':checked')) {

                    other_radios.find('.masked-radio-checked').remove();
                    $this.append('<div class="masked-radio-checked"></div>');
                }
                else {

                    $this.find('.masked-radio-checked').remove();
                }

            });
        });

        Courses.change(function () {
            //$("#courses-hidden").val($("option:selected", this).text());
            Hours.parent().hide();
            ChefHours.parent().hide();
            $("#courses-hidden").val($(this).parent().find("span").text());
            var Course = $("option:selected", this).text() == "Chef & Owner" ? 46 : Courses.val();
            if (Courses.val() != "45") {
                $("#chefcourses").hide();
                $("#chefcoursestitle").hide();
            }
            $.ajax({
                url: "/term/" + Course + "/0",
                dataType: 'json',
                async: false,
                success: function (data) {
                    Terms.find('option').remove().end().append(new Option("Seçiniz", ""));
                    Terms.next('.masked-select-text').text("Seçiniz");
                    Hours.find('label').remove();
                    jQuery.each(data, function (index, itemData) {
                        Terms.append(new Option(itemData.Title, itemData.BlockID));
                    });
                    FormSlider.redrawSlider();
                }
            });

        });

        Terms.change(function () {
            var Course = $("option:selected", this).text() == "Chef & Owner" ? 46 : Courses.val();
            if (Courses.val() == "45") {
                $("#chefcourses").show();
                $("#chefcoursestitle").show();
            } else {
                $("#chefcourses").hide();
                $("#chefcoursestitle").hide();
            }

            $.ajax({
                url: "/term/" + Course + "/" + Terms.val(),
                dataType: 'json',
                async: false,
                success: function (data) {
                    Hours.find('label').remove();
                    jQuery.each(data.Block, function (index, itemData) {

                        if (GetObject(itemData.Element, "Name", "term-status").Value == "false") {
                            Hours.append(RadioDisabled.replace("{0}", GetObject(itemData.Element, "Name", "term-title").Value + " - " + GetObject(itemData.Element, "Name", "term-hours").Value).replace("{1}", GetObject(itemData.Element, "Name", "term-status-message").Value));
                        } else {
                            Hours.append(Radio.replace("{0}", GetObject(itemData.Element, "Name", "term-title").Value + " - " + GetObject(itemData.Element, "Name", "term-hours").Value));
                        }
                    });

                    Hours.parent().show();
                    FormSlider.redrawSlider();
                }
            });

            Hour = $('input[name=hour]:radio');
            Hour.change(function () {
                $("#date-hidden").val($(this).parents("label").text());
                $("input[data-name=hour]").val($(this).parents("label").text());
            });

            $(".hidden-radio input").change(function () {
                var name = $(this).parent().parent().attr("data-name");
                var $this = $(this).parent().parent();
                var other_radios = $(document).find(".masked-radio[data-name='" + name + "']");

                if ($(this).is(':checked')) {

                    other_radios.find('.masked-radio-checked').remove();
                    $this.append('<div class="masked-radio-checked"></div>');
                }
                else {

                    $this.find('.masked-radio-checked').remove();
                }

            });
        });

        $("#country").change(function () {
            if ($(this).val() == "TR") {
                $("#city").parents("fieldset").show();
                $("#city").show();
            } else {
                $("#city").parents("fieldset").hide();
                $("#city").hide();
            }
        });

        $("#city").change(function () {
            $(this).parents("fieldset").next("fieldset").find(".phone-pre").val("0" + $(this).val());
        });
    }

    $("form .button").click(function (e) {
        var Fieldset = $(this).parents(".form-step").find($("fieldset.required:visible"));
        var Error = false;

        Fieldset.each(function (i, e) {
            $(e).removeClass("red");
            $("select,input,textarea", e).each(function (x, s) {
                if ($(s).val() == "") {
                    $(e).addClass("red");
                    Error = true;
                }
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if ($(s).hasClass("email") && !regex.test($(s).val())) {
                    $(e).addClass("red");
                    Error = true;
                }
            });
        });

        if (Error) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $(".red").offset().top
            }, 500);
            return;
        }
    });

    $(".phone-pre").keyup(function () {
        if ($(this).val().indexOf('_') == -1 && $(this).val().length == 4) {
            $(this).parents("fieldset").find(".phone-num").focus();
        }
    });

    var GetObject = function (obj, key, val) {
        var newObj = false;
        $.each(obj, function () {
            var testObject = this;
            $.each(testObject, function (k, v) {
                if (val == v && k == key) {
                    newObj = testObject;
                }
            });
        });

        return newObj;
    }


    var GetCourses = function (Age) {
        Courses.find('option').remove().end().append(new Option("Seçiniz", ""));
        if (Age >= 17 && Age <= 29) {
            Courses.append(new Option("Uzun Dönem Profesyonel Aşçılık", "38"));
            Courses.append(new Option("Uzun Dönem Profesyonel Pasta ve Ekmekçilik", "39"));
            Courses.append(new Option("Profesyonel Aşçılık", "40"));
            Courses.append(new Option("Profesyonel Pasta ve Ekmekçilik", "42"));
        }

        if (Age > 17 && Age <= 45) {
            Courses.append(new Option("Chef & Owner", "45"));
            Courses.append(new Option("ProChef", "44"));
        }

        if (Age >= 30 && Age <= 44) {
            Courses.append(new Option("Stajsız Profesyonel Aşçılık", "41"));
            Courses.append(new Option("Stajsız Profesyonel Pasta ve Ekmekçilik", "43"));
        }

        if (Age > 17 && Age <= 90) {
            Courses.append(new Option("Yiyecek - İçecek İşletmeciliği", "46"));
            Courses.append(new Option("Profesyonel Barmenlik ve Miksoloji", "47"));
        }
    }

    var GetAge = function (dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    var profesyonelEgitimler = $("#profesyonelEgitimler ul").bxSlider({
        mode: "horizontal",
        pager: true,
        controls: false
    });
    var amatorWorkshoplar = $("#amatorWorkshoplar ul").bxSlider({
        mode: "horizontal",
        pager: true,
        controls: false
    });
    var haftalikProgram = $(".haftalik-program ul").bxSlider({
        mode: "horizontal",
        pager: true,
        pagerType: "short",
        prevText: "",
        nextText: "",
        controls: true
    });


    $(".homepage-slider").swipe({
        swipeLeft: function () {
            swipeLeft(homeSlider)
        },
        swipeRight: function () {
            swipeRight(homeSlider)
        },
        threshold: 25
    });
    /*$(".haftalik-program").swipe({
       swipeLeft:function(){
           swipeLeft(haftalikProgram)
       },
       swipeRight:function(){
           swipeRight(haftalikProgram)
       },
       threshold:25
   });
   $(".egitim-kadrosu").swipe({
       swipeLeft:function(){
           swipeLeft(egitmenKadrosu)
       },
       swipeRight:function(){
           swipeRight(egitmenKadrosu)
       },
       threshold:25
   });*/

    function swipeLeft(slider) {
        slider.goToNextSlide();
    }
    function swipeRight(slider) {
        slider.goToPrevSlide();
    }

    $('.paragraph.short').readmore({
        maxHeight: 178,
        speed: 100,
        moreLink: '<a href="#">Devamını Oku</a>',
        lessLink: '<a href="#">Kapat</a>',
        embedCSS: true,
        sectionCSS: '',
        startOpen: false,
        expandedClass: 'readmore expanded',
        collapsedClass: 'readmore collapsed',
        beforeToggle: function () { },
        afterToggle: function () { }
    });




    if ($(".breadcrumps li").length > 1) {
        var width = 0;
        $(".breadcrumps li").each(function () {
            width += $(this).outerWidth(true);
        });
        $(".breadcrumps ul").css({
            width: width + 43 + "px"
        });
        var breadcrumps = new IScroll("#breadcrumps", {
            mouseWheel: true,
            scrollX: true,
            scrollY: false
        });
    }

    $(window).resize(function () {
        var height = $(window).height();
        $(".homepage-slider").height(height);
    }).trigger("resize");


    /*
	İletişim Formu
	******************************************************************/
    $("input.alphabetical").inputmask('Regex', { regex: "[ üğşöçıÜŞİÖÇa-zA-Z]{50}" });
    $("input.text-only").inputmask('Regex', { regex: "[ üğşöçıÜŞİÖÇa-zA-Z.-]{50}" });
    $("input.tc-no").inputmask("99999999999");
    $("input.tc-kimlik-no").inputmask("99999999999");
    $("input.date").inputmask("date");
    $("input.numeric").inputmask("decimal");
    $("input.gsm").inputmask("599 999 99 99");
    $("input.phone-pre").inputmask("0999");
    $("input.phone-num").inputmask("999 99 99");
    //$("input.email").inputmask('Regex', { regex: "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}" });
    $("input.price").inputmask("9.999 TL");

});//ready